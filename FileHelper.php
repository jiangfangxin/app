<?php

require_once 'DbHelper.php';

class FileHelper
{
//    public static $uploadPath = '/var/www/example.com/data';
    public static $uploadPath = __DIR__ . '/data';

    public static function saveDataFile($equipmentId, $file, $fileName)
    {
        $shop = DbHelper::getShop($equipmentId);
        if ($shop === false) {
            return false;
        }
        $shopId = $shop['id'];
        $businessId = $shop['business_id'];

        $uploadDir = self::$uploadPath . '/' . $businessId. '/' . $shopId . '/' . $equipmentId;
        if (!is_dir($uploadDir)) {
            try {
                mkdir($uploadDir, 0777, true);
            } catch(\Exception $e) {
                return false;
            }
        }
        $uploadfile = $uploadDir . '/' . $fileName;

        return move_uploaded_file($_FILES[$file]['tmp_name'], $uploadfile);
    }
}