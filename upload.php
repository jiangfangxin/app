<?php

require_once 'DbHelper.php';
require_once 'FileHelper.php';

/**
 * URL: equipmentId, type, time, file, authString
 */

header('Content-type: application/json');

function uploadData()
{
    /**** 设备ID部分 ****/
    if (!isset($_GET['equipmentId'])) return '{"result":false,"status":"URL中没有设备ID"}';
    $equipmentId = $_GET['equipmentId'];

    $equipment = DbHelper::getEquipment($equipmentId);
    if (!$equipment) return '{"result":false,"status":"数据库中没有此设备ID!"}';


    /**** 文件类型部分 ****/
    if (!isset($_GET['type'])) return '{"result":false,"status":"URL中没有数据类型!"}';
    $type = $_GET['type'];

    // 数据文件类型验证
    if (!in_array($type, ['debug_log', 'user_log'])) return '{"result":false,"status":"数据类型不正确!"}';


    /**** 时间部分 ****/
    if (!isset($_GET['time'])) return '{"result":false,"status":"URL中没有提供时间!"}';
    $time = $_GET['time'];


    /**** 签名部分 ****/
    if (!isset($_GET['authString'])) return '{"result":false,"status":"URL中没有签名!"}';
    $authString = $_GET['authString'];

    // HMAC检验
    $query = 'equipmentId=' . $equipmentId . '&type=' . $type . '&time=' . $time;
    $check = hash_hmac("sha1", $query, $equipment['auth_key']);
    if ($authString != $check) return '{"result":false,"status":"签名不正确!"}';


    /**** 判断接口访问时间是否小于5分钟 ****/
    $now = time();
    if ($equipment['last_time']) {
        $last = strtotime($equipment['last_time']);
        $limit = 5 * 60;
        if ($now - $last < $limit) return '{"result":false,"status":"接口访问应该间隔5分钟!"}';
    }

    // 更新接口访问时间
    DbHelper::updateEquipmentAccessTime($equipmentId, date('Y-m-d H:i:s', $now));


    /**** 文件部分 ****/
    if (!isset($_FILES['file']) || $_FILES['file']['error'] != 0) return '{"result":false,"status":"没有上传数据文件!"}';
    if ($_FILES['file']['size'] > 5 * 1024 * 1024) return '{"result":false,"status":"数据文件不能超过5M!"}';
    $file = file_get_contents($_FILES['file']['tmp_name']);


    // 保存数据文件
    $fileName = $type . '-' . date('YmdHis', strtotime($time));
    if (FileHelper::saveDataFile($equipmentId, 'file', $fileName) && DbHelper::insertData($equipmentId, $type, $fileName)) {
        return '{"result":true,"status":"数据文件保存成功!"}';
    } else {
        return '{"result":false,"status":"保存出现异常!"}';
    }
}

echo uploadData();





