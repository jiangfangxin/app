-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema app
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `app` ;

-- -----------------------------------------------------
-- Schema app
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `app` DEFAULT CHARACTER SET utf8 ;
USE `app` ;

-- -----------------------------------------------------
-- Table `app`.`business`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `app`.`business` ;

CREATE TABLE IF NOT EXISTS `app`.`business` (
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = '商户';


-- -----------------------------------------------------
-- Table `app`.`shop`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `app`.`shop` ;

CREATE TABLE IF NOT EXISTS `app`.`shop` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `business_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_shop_business_idx` (`business_id` ASC),
  CONSTRAINT `fk_shop_business`
    FOREIGN KEY (`business_id`)
    REFERENCES `app`.`business` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '分店';


-- -----------------------------------------------------
-- Table `app`.`equipment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `app`.`equipment` ;

CREATE TABLE IF NOT EXISTS `app`.`equipment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `auth_key` VARCHAR(45) NOT NULL COMMENT '验证字符串',
  `last_time` DATETIME NULL COMMENT '最后一次成功接口调用时间',
  `shop_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_equipment_shop1_idx` (`shop_id` ASC),
  CONSTRAINT `fk_equipment_shop1`
    FOREIGN KEY (`shop_id`)
    REFERENCES `app`.`shop` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '设备';


-- -----------------------------------------------------
-- Table `app`.`data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `app`.`data` ;

CREATE TABLE IF NOT EXISTS `app`.`data` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL COMMENT '数据类型',
  `file_name` VARCHAR(45) NOT NULL COMMENT '文件名',
  `equipment_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_data_equipment1_idx` (`equipment_id` ASC),
  CONSTRAINT `fk_data_equipment1`
    FOREIGN KEY (`equipment_id`)
    REFERENCES `app`.`equipment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '数据';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `app`.`business`
-- -----------------------------------------------------
START TRANSACTION;
USE `app`;
INSERT INTO `app`.`business` (`id`) VALUES (1);
INSERT INTO `app`.`business` (`id`) VALUES (2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `app`.`shop`
-- -----------------------------------------------------
START TRANSACTION;
USE `app`;
INSERT INTO `app`.`shop` (`id`, `business_id`) VALUES (1, 1);
INSERT INTO `app`.`shop` (`id`, `business_id`) VALUES (2, 1);
INSERT INTO `app`.`shop` (`id`, `business_id`) VALUES (3, 2);
INSERT INTO `app`.`shop` (`id`, `business_id`) VALUES (4, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `app`.`equipment`
-- -----------------------------------------------------
START TRANSACTION;
USE `app`;
INSERT INTO `app`.`equipment` (`id`, `auth_key`, `last_time`, `shop_id`) VALUES (1, '1sadfwdfsdfew', NULL, 1);
INSERT INTO `app`.`equipment` (`id`, `auth_key`, `last_time`, `shop_id`) VALUES (2, '2sdfsfgbsdfew', NULL, 1);
INSERT INTO `app`.`equipment` (`id`, `auth_key`, `last_time`, `shop_id`) VALUES (3, '3dsfrhsdfsdfe', NULL, 2);
INSERT INTO `app`.`equipment` (`id`, `auth_key`, `last_time`, `shop_id`) VALUES (4, '4sdfgsddvdsh', NULL, 3);
INSERT INTO `app`.`equipment` (`id`, `auth_key`, `last_time`, `shop_id`) VALUES (5, '5sdgsedasvzz', NULL, 4);

COMMIT;

