<?php
/**
 * 数据库链接类
 */
class Connection
{
    /**
     * @var string PDO的dsn参数
     */
    private static $dsn = 'mysql:host=localhost;dbname=app;charset=utf8';
    /**
     * @var string 用户名
     */
    private static $username = 'root';
    /**
     * @var string 密码
     */
    private static $password = '';
    /**
     * @var null|\PDO PDO对象
     */
    private static $_pdo = null;

    public static function getPdo()
    {
        if (!self::$_pdo) {
            self::$_pdo = new \PDO(self::$dsn, self::$username, self::$password);
        }
        return self::$_pdo;
    }
}