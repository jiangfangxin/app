<?php
require_once 'Connection.php';

class DbHelper
{
    public static function getAllEquipments()
    {
        $sql = "SELECT * FROM `equipment` WHERE 1";
        $pdo = Connection::getPdo();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->execute();
        $rows = $pdoStatement->fetchAll(\PDO::FETCH_ASSOC);
        return $rows;
    }

    public static function getEquipment($equipmentId)
    {
        $sql = "SELECT * FROM `equipment` WHERE `id`=:equipmentId";
        $pdo = Connection::getPdo();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->bindParam(':equipmentId', $equipmentId, PDO::PARAM_INT);
        $pdoStatement->execute();
        $row = $pdoStatement->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }

    public static function updateEquipmentAccessTime($equipmentId, $time)
    {
        $sql = "UPDATE `equipment` SET `last_time`=:time WHERE `id`=:equipmentId";
        $pdo = Connection::getPdo();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->bindParam(':time', $time, PDO::PARAM_STR);
        $pdoStatement->bindParam(':equipmentId', $equipmentId, PDO::PARAM_INT);
        return $pdoStatement->execute();
    }

    public static function getShop($equipmentId)
    {
        $row = self::getEquipment($equipmentId);
        if ($row === false) {
            return false;
        }
        $shopId =  $row['shop_id'];
        $sql = "SELECT * FROM `shop` WHERE `id`=:shopId";
        $pdo = Connection::getPdo();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->bindParam(':shopId', $shopId, PDO::PARAM_INT);
        $pdoStatement->execute();
        $row = $pdoStatement->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }

    public static function insertData($equipmentId, $type, $fileName)
    {
        $sql = "INSERT INTO `data` (`type`, `file_name`, `equipment_id`) VALUES (:type, :fileName, :equipmentId)";
        $pdo = Connection::getPdo();
        $pdoStatement = $pdo->prepare($sql);
        $pdoStatement->bindParam(':type', $type, PDO::PARAM_STR);
        $pdoStatement->bindParam(':fileName', $fileName, PDO::PARAM_STR);
        $pdoStatement->bindParam(':equipmentId', $equipmentId, PDO::PARAM_INT);
        return $pdoStatement->execute();
    }
}