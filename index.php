<?php

require_once 'DbHelper.php';

$equipments = DbHelper::getAllEquipments();
$sorted = [];
foreach ($equipments as $equipment) {
    $sorted[$equipment['id']] = $equipment;
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>App</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <script src="js/jquery.js"></script>
    <script type="text/javascript" src="js/2.0.0-crypto-sha1.js"></script>
    <script type="text/javascript" src="js/2.0.0-hmac-min.js"></script>
</head>
<body>
    <div style="width:80%;margin:20px auto;">
        <p>URL</p>
        <form id="form1" role="form" class="form form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">设备ID</label>
                <div class="col-sm-5">
                    <select id="equipmentId" class="form-control" name="equipmentId"></select>
                </div>

                <label class="col-sm-2 control-label">签名密钥</label>
                <div id="authKey" class="col-sm-2"></div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">数据类型</label>
                <div class="col-sm-10">
                    <select class="form-control" name="type">
                        <option value="debug_log">debug log</option>
                        <option value="user_log">商户操作log</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">当前时间</label>
                <div class="col-sm-10">
                    <input type="text" id="time" class="form-control" name="time">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">HMAC签名</label>
                <div id="authString" class="col-sm-10"></div>
            </div>
        </form>

        <p>POST</p>
        <form id="form2" role="form" class="form form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">数据文件</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" name="file">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2">
                    <button id="button" type="button" class="btn btn-primary">提交</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row" style="width:80%;margin:20px auto;">
        <div id="result" class="col-sm-offset-2">

        </div>
    </div>

    <script type="text/javascript">
        var equipments = <?= json_encode($sorted) ?>;
        var $equipmentIdField =  $('#equipmentId');
        for (var equipmentId in equipments) {
            $equipmentIdField.append('<option value="' + equipmentId + '">' + equipmentId + '</option>');
        }
        $('#authKey').html(equipments[$equipmentIdField.val()].auth_key);

        $equipmentIdField.change(function () {
            $('#authKey').html(equipments[this.value].auth_key);
        });

        $('#button').click(function() {
            var arr = $(form1).serializeArray();
            var query = '';
            var sep = '';
            for (var i in arr) {
                query += sep + arr[i].name + '=' + arr[i].value;
                sep = '&';
            }
            var authString = Crypto.HMAC(Crypto.SHA1, query, $('#authKey').text());
            $('#authString').html(authString);
            query = query + '&authString=' + authString;
            var formData = new FormData(document.forms[1]);
            var hmac = Crypto.HMAC(Crypto.SHA1, "Message", "Secret Passphrase");
            formData.set('authString', authString);
            $.ajax({
                url: 'upload.php?' + query,
                type: "post",
                data: formData,
                dataType: "json",
                success: function(result) {
                    $('#result').html(result.status);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#result').html(jqXHR.responseText);
                },
                contentType: false,
                processData: false
            });
        });
        setInterval(function () {
            var date = new Date();
            $('#time').val(date.getFullYear() + '-' + date.getMonth() + '-' + date.getDay() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds());
        }, 1000)
    </script>
</body>
</html>